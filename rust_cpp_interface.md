
# State of the rust-cpp interface:

There are multiple ways to call c++ code from rust code. Here, I want to list those and name their advantages and disadvantages:

## Methods that do not require us to use the "unsafe" keyword:

[autocxx](https://github.com/google/autocxx): Currently (August 2020)  at a very early state (for example, only ints and voids are supported) but looks promising (It is from google).

[cxx](https://github.com/dtolnay/cxx): Uses unique pointers alot. Does not allow operator overloading (afaik).

## Methods that require us to use the "unsafe" keyword:

[bindgen](https://github.com/rust-lang/rust-bindgen): Works fine if you want to call C-Code, but has poor C++, support, you probably have to write a C-Wrapper. You need to write a wrapper function if you want to call a C++ overloaded operator.

[rustcxx](https://github.com/google/rustcxx): This provided the cxx!(src) macro. This will compile the argument as C++ code.

```rust
	let mut x = 0u32;
	unsafe {
        cxx![(x: *mut u32 = &mut x) {
			std::cout << x;
            std::cin >> *x;
        }];
	}
```
[rust-cpp](https://github.com/mystor/rust-cpp): Similar to rustcxx.

```rust
let r = unsafe {
        cpp!([name_ptr as "const char *"] -> u32 as "int32_t" {
            std::cout << "Hello, " << name_ptr << std::endl;
            return 42;
        })
    };
```